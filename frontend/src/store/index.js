// noinspection NpmUsedModulesInstalled
import Vue from 'vue'
import Vuex from 'vuex'

import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

let state = {
  layoutNeeded: true,
  updateNeeded: false,
  refreshNeeded: true,
  eventsRegistered: false,
  isLoginPage: false,
  user: '',
  mobileMode: false,
  menuCollapse: true,
  numTweets: 20,
  scrapAll: false,
  auth: sessionStorage.getItem('jwt') !== null,
  tweets: [],
  groupedTweets: [],
  viewedTweets: [],
  assignedAccounts: [],
  updateMessages: [],
  timerInterval: 300000,
  timerIntervalId: null
}

// noinspection JSUnusedGlobalSymbols
let mutations = {
  setLayoutNeeded (state, value) {
    state.layoutNeeded = value
  },
  setUpdateNeeded (state, value) {
    state.updateNeeded = value
  },
  setRefreshNeeded (state, value) {
    state.refreshNeeded = value
  },
  setEventsRegistered (state, value) {
    state.eventsRegistered = value
  },
  setScrapAll (state, value) {
    state.scrapAll = value
  },
  setUser (state, value) {
    state.user = value
  },
  setIsLoginPage (state, value) {
    state.isLoginPage = value
  },
  setMobileMode (state, value) {
    state.mobileMode = value
  },
  setMenuCollapse (state, value) {
    state.menuCollapse = value
  },
  setTweets (state, tweets) {
    state.tweets = tweets
  },
  setGroupedTweets (state, tweets) {
    function groupBy (list, keyGetter) {
      const map = new Map()
      list.forEach((item) => {
        const key = keyGetter(item)
        const collection = map.get(key)
        if (!collection) {
          map.set(key, [item])
        } else {
          collection.push(item)
        }
      })
      return map
    }
    let tweeterAccounts = tweets.map(t => ({user: t.user.split(' ')[0], fullName: t.fullName, imageUrl: t.imageUrl, url: t.url}))
    state.groupedTweets = Array.from(groupBy(tweeterAccounts, t => t.user))
  },
  setAuth (state, value) {
    state.auth = value
  },
  setAssignedAccounts (state, accounts) {
    state.assignedAccounts = accounts
  },
  setUpdateMessages (state, message) {
    state.updateMessages.push(message)
  },
  setTimerInterval (state, interval) {
    state.timerInterval = interval * 1000
  },
  setTimerIntervalId (state, intervalId) {
    state.timerIntervalId = intervalId
  },
  setViewedTweets (state, tweets) {
    state.viewedTweets = state.viewedTweets.concat(tweets)
  },
  setNumTweets (state, numTweets) {
    state.numTweets = numTweets
  }
}

// noinspection JSUnusedGlobalSymbols
let getters = {
  getLayoutNeeded () {
    return state.layoutNeeded
  },
  getUpdateNeeded () {
    return state.updateNeeded
  },
  getRefreshNeeded () {
    return state.refreshNeeded
  },
  getEventsRegistered () {
    return state.eventsRegistered
  },
  getScrapAll () {
    return state.scrapAll
  },
  getUser () {
    return state.user
  },
  getIsLoginPage () {
    return state.isLoginPage
  },
  getMobileMode () {
    return state.mobileMode
  },
  getMenuCollapse () {
    return state.menuCollapse
  },
  getTweets () {
    return state.tweets
  },
  getGroupedTweets () {
    return state.groupedTweets
  },
  getAuth () {
    return state.auth
  },
  getAssignedAccounts () {
    return state.assignedAccounts
  },
  getUpdateMessages () {
    return state.updateMessages
  },
  getTimerInterval () {
    return state.timerInterval
  },
  getTimerIntervalId () {
    return state.timerIntervalId
  },
  getViewedTweets () {
    return state.viewedTweets
  },
  getNumTweets () {
    return state.numTweets
  }
}

export default function (/* { ssrContext } */) {
  // noinspection JSValidateTypes
  return new Vuex.Store({
    modules: {
      example
    },
    state,
    mutations,
    getters
  })
}
