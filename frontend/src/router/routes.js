
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    meta: { requiresAuth: true },
    children: [
      { path: '', component: () => import('pages/Index.vue'), meta: { requiresAuth: true } },
      { path: '/stats', component: () => import('pages/Charts.vue'), meta: { requiresAuth: true } },
      { path: '/accounts', component: () => import('pages/TweeterAccounts.vue'), meta: { requiresAuth: true } }
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/Auth.vue'),
    children: [
      { path: '/login', component: () => import('pages/Login.vue') },
      { path: '/register', component: () => import('pages/Register.vue') },
      { path: '/restore', component: () => import('pages/Restore.vue') },
      { path: '/changepassword', component: () => import('pages/NewPassword.vue') },
      { path: '/token', component: () => import('pages/Token.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
