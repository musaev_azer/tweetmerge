export const localStorageKeys = {
  LANGUAGE_KEY: 'qtwitop.app.lang',
  TIMER_INTERVAL_KEY: 'qtwitop.app.timer.interval',
  ANIMATION_TYPE_KEY: 'qtwitop.app.animation.type',
  VIEW_FRAME_SIZE_KEY: 'qtwitop.app.view.frame.size',
  SCRAP_ALL_TWEETS_KEY: 'qtwitop.app.scrap.all.tweets',
  USER_KEY: 'qtwitop.app.user'
}
