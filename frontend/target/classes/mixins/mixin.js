import Vivus from 'vivus'
// noinspection NpmUsedModulesInstalled
import {QSpinnerFacebook} from 'quasar'
import moment from 'moment'
import { mapGetters, mapMutations } from 'vuex'
import { localStorageKeys } from '../helpers/localStorageKeys'

export default {
  methods: {
    ...mapGetters(['getTweets', 'getGroupedTweets', 'getViewedTweets', 'getAssignedAccounts', 'getUpdateNeeded', 'getRefreshNeeded']),
    ...mapMutations(['setAssignedAccounts', 'setUpdateNeeded', 'setRefreshNeeded', 'setUser']),
    startAnimation (logo) {
      this.vivus = new Vivus(logo, {
        duration: 1100,
        forceRender: true,
        animTimingFunction: Vivus.LINEAR
      }, function (element) {
        for (let item of element.el.children[0].children) {
          item.setAttribute('style', 'fill:white')
          item.setAttribute('style', 'fill:white')
        }
      }
      )
    },
    notifySnack (message, details, type, icon) {
      // noinspection JSUnresolvedVariable
      this.$q.notify({
        message: message,
        timeout: 0,
        type: type,
        color: type,
        textColor: 'black',
        icon: icon,
        detail: details,
        position: 'bottom',
        actions: [
          {
            icon: 'close',
            label: this.$i18n.t('dismiss'),
            handler: () => {}
          }
        ]
      })
    },
    showLoading (message) {
      // noinspection JSUnresolvedVariable
      this.$q.loading.setDefaults({
        spinner: QSpinnerFacebook,
        message: message,
        messageColor: 'white',
        spinnerSize: 140,
        spinnerColor: 'amber',
        customClass: 'bg-primary'
      })
      this.$q.loading.show({ delay: 0 })
    },
    hideLoading () {
      this.$q.loading.hide()
    },
    getDateNowFormatted () {
      return moment().format('MMMM Do YYYY, h:mm:ss a')
    },
    clearBeforeLogout () {
      sessionStorage.removeItem('jwt')
      this.setAuth(false)
      this.getTweets.length = 0
      this.getViewedTweets.length = 0
      this.getUpdateMessages.length = 0
      this.getGroupedTweets.length = 0
      this.setUser('')
      this.$q.localStorage.remove(localStorageKeys.USER_KEY)
      this.$root.$emit('stop-listening')
    }
  }
}
