import Vuelidate from 'vuelidate'

// noinspection JSUnusedGlobalSymbols
export default ({ Vue }) => {
  Vue.use(Vuelidate)
}
