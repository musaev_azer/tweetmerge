import axios from 'axios'

axios.interceptors.request.use(function (config) {
  const jwtToken = sessionStorage.getItem('jwt')
  if (jwtToken !== null) {
    config.headers['Authorization'] = jwtToken
  }
  return config
}, function (error) {
  return Promise.reject(error)
})
// noinspection JSUnusedGlobalSymbols
export default ({ Vue }) => {
  // noinspection JSUnusedGlobalSymbols
  Vue.prototype.$axios = axios
}
