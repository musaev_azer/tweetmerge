const languagesNoVoice = [
  'az', 'eu', 'be', 'bn', 'bg', 'ceb', 'et', 'tl', 'gl', 'ka', 'gu', 'ha', 'iw', 'hmn',
  'ig', 'ga', 'jw', 'kn', 'km', 'lo', 'lt', 'ms', 'mt', 'mi', 'mr', 'mn', 'ne', 'fa',
  'pa', 'sl', 'so', 'te', 'uk', 'ur', 'yi', 'yo', 'zu', 'si', 'st'
]

const languagesMapName = {
  'bg': 'Bulgarian',
  'ca': 'Catalan',
  'ceb': 'Cebuano',
  'zh-CN': 'Ch (Simplified)',
  'zh-TW': 'Ch (Traditional)',
  'hr': 'Croatian',
  'cs': 'Czech',
  'af': 'Afrikaans',
  'sq': 'Albanian',
  'ar': 'Arabic',
  'hy': 'Armenian',
  'az': 'Azerbaijani',
  'eu': 'Basque',
  'be': 'Belarusian',
  'bn': 'Bengali',
  'th': 'Thai',
  'tr': 'Turkish',
  'uk': 'Ukrainian',
  'ur': 'Urdu',
  'vi': 'Vietnamese',
  'cy': 'Welsh',
  'yi': 'Yiddish',
  'yo': 'Yoruba',
  'zu': 'Zulu',
  'ka': 'Georgian',
  'de': 'German',
  'el': 'Greek',
  'gu': 'Gujarati',
  'ht': 'Haitian Creole',
  'ha': 'Hausa',
  'iw': 'Hebrew',
  'hi': 'Hindi',
  'hmn': 'Hmong',
  'da': 'Danish',
  'nl': 'Dutch',
  'en': 'English',
  'eo': 'Esperanto',
  'et': 'Estonian',
  'tl': 'Filipino',
  'fi': 'Finnish',
  'fr': 'French',
  'gl': 'Galician',
  'hu': 'Hungarian',
  'is': 'Icelandic',
  'ig': 'Igbo',
  'id': 'Indonesian',
  'ga': 'Irish',
  'it': 'Italian',
  'ja': 'Japanese',
  'jw': 'Javanese',
  'kn': 'Kannada',
  'mr': 'Marathi',
  'mn': 'Mongolian',
  'ne': 'Nepali',
  'no': 'Norwegian',
  'fa': 'Persian',
  'pl': 'Polish',
  'pt': 'Portuguese',
  'pa': 'Punjabi',
  'ro': 'Romanian',
  'km': 'Khmer',
  'ko': 'Korean',
  'lo': 'Lao',
  'la': 'Latin',
  'lv': 'Latvian',
  'lt': 'Lithuanian',
  'mk': 'Macedonian',
  'ms': 'Malay',
  'mt': 'Maltese',
  'mi': 'Maori',
  'ru': 'Russian',
  'sr': 'Serbian',
  'sk': 'Slovak',
  'sl': 'Slovenian',
  'so': 'Somali',
  'es': 'Spanish',
  'sw': 'Swahili',
  'sv': 'Swedish',
  'ta': 'Tamil',
  'te': 'Telugu'
}

function m(i) {
  var tmp = [
    "aHR0cDovL2FkZDBuLmNvbS9nb29nbGUtdHJhbnNsYXRvci5odG1sP3ZlcnNpb249",
    'aHR0cDovL3RyYW5zbGF0ZS5nb29nbGUuY29tL3RyYW5zbGF0ZV9hL3Q/Y2xpZW50PXAmc2w9',
    "aHR0cHM6Ly90cmFuc2xhdGUuZ29vZ2xlLmNvbS8j",
    'aHR0cHM6Ly90cmFuc2xhdGUuZ29vZ2xlLmNvbS90cmFuc2xhdGVfdHRzP2llPVVURi04JnE9',
    'aHR0cHM6Ly90cmFuc2xhdGUuZ29vZ2xlLmNvbS90cmFuc2xhdGVfYS9zZz9jbGllbnQ9dCZjbT0=',
    'aHR0cHM6Ly90cmFuc2xhdGUuZ29vZ2xlLmNvbS90cmFuc2xhdGU/cHJldj1fdCZobD1lbiZpZT1VVEYtOCZ1PQ==',
    'aHR0cHM6Ly90cmFuc2xhdGUuZ29vZ2xlLmNvbS90cmFuc2xhdGVfYS9zaW5nbGU/Y2xpZW50PXQmc2w9',
    "aHR0cDovL2FkZDBuLmNvbS9mZWVkYmFjay5odG1s"
  ];
  return tmp[i] ? atob(tmp[i]) : '';
}

window.addEventListener("load", function () {
  window.postMessage({'TKK': TKK}, '*')
});

window.addEventListener("message", function (e) {
  if (e.data && "TKK" in e.data) {
    window.TKK = e.data["TKK"];
  }
}, false);

let calcToken = function (a) {
  let k = "", cb = "&", Gf = "=", jd = ".", $b = "+-a^+6", t = "a", Yb = "+", Zb = "+-3^+b+-f";
  let sM = function (a) {return function () {return a}},
    tM = function (a, b) {
      for (var c = 0; c < b.length - 2; c += 3) {
        var d = b.charAt(c + 2), d = d >= t ? d.charCodeAt(0) - 87 : Number(d), d = b.charAt(c + 1) === Yb ? a >>> d : a << d;
        a = b.charAt(c) === Yb ? a + d & 4294967295 : a ^ d
      }
      return a
    },
    uM = null;
  /*  */
  let b;
  if (null !== uM) b = uM;
  else {
    b = sM(String.fromCharCode(84));
    let c = sM(String.fromCharCode(75));
    b = [b(), b()];
    b[1] = c();
    b = (uM = window.TKK || k) || k;
  }
  /*  */
  var d = sM(String.fromCharCode(116)), c = sM(String.fromCharCode(107)), d = [d(), d()];
  d[1] = c();
  c = cb + d.join(k) + Gf;
  d = b.split(jd);
  b = Number(d[0]) || 0;
  for (var e = [], f = 0, g = 0; g < a.length; g++) {
    var m = a.charCodeAt(g);
    128 > m ? e[f++] = m : (2048 > m ? e[f++] = m >> 6 | 192 : (55296 === (m & 64512) && g + 1 < a.length && 56320 === (a.charCodeAt(g + 1) & 64512) ? (m = 65536 + ((m & 1023) << 10) + (a.charCodeAt(++g) & 1023), e[f++] = m >> 18 | 240, e[f++] = m >> 12 & 63 | 128) : e[f++] = m >> 12 | 224, e[f++] = m >> 6 & 63 | 128), e[f++] = m & 63 | 128)
  }
  /*  */
  a = b;
  for (f = 0; f < e.length; f++) a += e[f], a = tM(a, $b);
  a = tM(a, Zb);
  a ^= Number(d[1]) || 0;
  0 > a && (a = (a & 2147483647) + 2147483648);
  a %= 1E6;
  return (a.toString() + jd + (a ^ b));
};
