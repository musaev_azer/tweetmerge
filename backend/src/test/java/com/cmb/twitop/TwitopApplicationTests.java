package com.cmb.twitop;

import com.cmb.twitop.dao.PrivilegeRepository;
import com.cmb.twitop.dao.RoleRepository;
import com.cmb.twitop.dao.UserRepository;
import com.cmb.twitop.model.Privilege;
import com.cmb.twitop.model.Role;
import com.cmb.twitop.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TwitopApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void contextLoads() {
    }

    @Test
    public void testAuthentication() throws Exception {
        this.mockMvc.perform(get("/login")
                .content("{\"email\":\"test@test.com\", \"password\":\"test\"}"))
                .andDo(print())
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/login")
                .content("{\"email\":\"admin@test.com\", \"password\":\"admin\"}"))
                .andDo(print())
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void testPasswordEncoder() {
        String encode = this.passwordEncoder.encode("test");
        assertNotNull(encode);
        assertTrue(encode.startsWith("$2a$11"));
    }

    @Test
    public void findAllUsers() {
        List<User> users = userRepository.findAll();
        assertNotNull(users);
        assertTrue(!users.isEmpty());
    }

    @Test
    public void createUser() {
        User user = new User();
        user.setEmail("ismaz@mail.ru");
        user.setPassword(passwordEncoder.encode("password123098"));
        user.setEnabled(true);

        roleRepository.save(new Role("Users"));
        roleRepository.save(new Role("Admins"));

        user.setRoles(new ArrayList<Role>() {
            {
                add(roleRepository.findByName("Users"));
                add(roleRepository.findByName("Admins"));
            }
        });
        User savedUser = userRepository.save(user);
        assertNotNull(savedUser);
        Optional<User> optionalUser = userRepository.findById(savedUser.getId());
        assertTrue(optionalUser.isPresent());
        assertEquals(savedUser.getEmail(), optionalUser.get().getEmail());
        assertEquals(savedUser.toString(), optionalUser.get().toString());
    }

    @Test
    public void createPrivilege() {
        Privilege read_privilege = new Privilege("READ_PRIVILEGE");
        read_privilege.setRoles(new ArrayList<>(Collections.singletonList(roleRepository.findByName("Users"))));
        Privilege savedPrivilege = privilegeRepository.save(read_privilege);
        assertNotNull(savedPrivilege);
        assertEquals(read_privilege.getName(), savedPrivilege.getName());
    }

}

