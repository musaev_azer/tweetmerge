package com.cmb.twitop.controller;

import com.cmb.twitop.dao.UserRepository;
import com.cmb.twitop.dto.TwitterAccountsDto;
import com.cmb.twitop.model.Tweet;
import com.cmb.twitop.model.TweeterAccount;
import com.cmb.twitop.model.User;
import com.cmb.twitop.service.IUserService;
import com.cmb.twitop.util.QuickTweetsExtractor;
import com.cmb.twitop.util.TasksExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/*
 * Created by Azer on 1/15/2019.
 * Home Inc.
 */
@SuppressWarnings({"LambdaBodyCanBeCodeBlock", "unchecked", "Duplicates"})
@RestController
public class TweetController {
    private final static Logger logger = LoggerFactory.getLogger(TweetController.class);
    private final IUserService userService;
    private final UserRepository userRepository;

    @Autowired
    public TweetController(IUserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/api/tweets", method = RequestMethod.GET)
    public List<Tweet> getTweets(final HttpServletRequest request, @RequestParam("scrapAll") boolean scrapAll) {
        Principal userPrincipal = request.getUserPrincipal();
        return TasksExecutor.getTweets(userPrincipal, userService, scrapAll);
    }

    @RequestMapping(value = "/api/accounts", method = RequestMethod.GET)
    public List<TweeterAccount> getAccounts(final HttpServletRequest request) {
        Principal userPrincipal = request.getUserPrincipal();
        User user = userService.findUserByEmail(userPrincipal.getName());
        return (List<TweeterAccount>) user.getTweeterAccounts();
    }

    @RequestMapping(value = "/api/delete_account", method = RequestMethod.POST)
    public List<TweeterAccount> deleteAccount(@RequestParam("account") String account, final HttpServletRequest request) {
        Principal userPrincipal = request.getUserPrincipal();
        User user = userService.findUserByEmail(userPrincipal.getName());
        Collection<TweeterAccount> tweeterAccounts = user.getTweeterAccounts();
        Optional<TweeterAccount> tweeterAccount = user.getTweeterAccounts().stream()
                .filter(t -> t.getUser().equals(account)).findFirst();
        if (tweeterAccount.isPresent()) {
            tweeterAccounts.remove(tweeterAccount.get());
            user = userRepository.save(user);
        }
        return (List<TweeterAccount>) user.getTweeterAccounts();
    }

    @RequestMapping(value = "/api/refresh_accounts", method = RequestMethod.GET)
    public List<TweeterAccount> refreshAccounts(final HttpServletRequest request) {
        Principal userPrincipal = request.getUserPrincipal();
        User user = userService.findUserByEmail(userPrincipal.getName());
        List<String> accounts = user.getTweeterAccounts().stream().map(TweeterAccount::getUser)
                .collect(Collectors.toList());
        if(accounts.size() == 0)
            return new ArrayList<>();
        List<TweeterAccount> tweeterAccounts = getTweeterAccounts(accounts);
        user.getTweeterAccounts().clear();
        userRepository.save(user);
        for (TweeterAccount tweeterAccount : tweeterAccounts) user.addTweeterAccount(tweeterAccount);
        user = userRepository.save(user);
        return (List<TweeterAccount>) user.getTweeterAccounts();
    }

    @RequestMapping(value = "/api/accounts", method = RequestMethod.POST)
    public List<TweeterAccount> addAccounts(final @Valid TwitterAccountsDto twitterAccountsDto, final HttpServletRequest request) {
        List<String> accounts = Arrays.asList(twitterAccountsDto.getAccounts());
        Principal userPrincipal = request.getUserPrincipal();
        User user = userService.findUserByEmail(userPrincipal.getName());
        List<TweeterAccount> tweeterAccounts = getTweeterAccounts(accounts);
        for (TweeterAccount tweeterAccount : tweeterAccounts) user.addTweeterAccount(tweeterAccount);
        user = userRepository.save(user);
        return (List<TweeterAccount>) user.getTweeterAccounts();
    }

    private List<TweeterAccount> getTweeterAccounts(List<String> accounts) {
        long currentTimeMillis = System.currentTimeMillis();
        ExecutorService executorService = Executors.newFixedThreadPool(accounts.size());
        Collection<Callable<TweeterAccount>> tasks = new ArrayList<>();
        accounts.forEach(u -> tasks.add(new QuickTweetsExtractor(u)));
        List<Future<TweeterAccount>> futures = null;
        try {
            futures = executorService.invokeAll(tasks);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
        List<TweeterAccount> tweeterAccounts = Objects.requireNonNull(futures).stream().map(f -> {
            try {
                return f.get();
            } catch (InterruptedException | ExecutionException e) {
                logger.error(e.getMessage());
            }
            return null;
        }).collect(Collectors.toList());
        float v = (System.currentTimeMillis() - currentTimeMillis) / 1000F;
        logger.info("Total {} tweeter accounts in {} seconds", tweeterAccounts.size(), v);
        return tweeterAccounts;
    }
}
