package com.cmb.twitop.controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Azer on 3/26/2019.
 * Home Inc.
 */
@SuppressWarnings("unused")
@EnableAutoConfiguration
@Controller
public class HtmlController {
    private final static Logger logger = LoggerFactory.getLogger(HtmlController.class);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(final HttpServletRequest request) {
        return "index";
    }
}
