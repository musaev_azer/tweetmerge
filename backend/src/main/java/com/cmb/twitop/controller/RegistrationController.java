package com.cmb.twitop.controller;

import com.cmb.twitop.dto.PasswordDto;
import com.cmb.twitop.dto.UserDto;
import com.cmb.twitop.exception.TokenNotFoundException;
import com.cmb.twitop.exception.UserNotFoundException;
import com.cmb.twitop.model.Privilege;
import com.cmb.twitop.model.Role;
import com.cmb.twitop.model.User;
import com.cmb.twitop.registration.OnRegistrationCompleteEvent;
import com.cmb.twitop.registration.OnSendTokenEvent;
import com.cmb.twitop.service.IUserService;
import com.cmb.twitop.util.GenericResponse;
import com.cmb.twitop.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@SuppressWarnings({"unused", "SpringJavaAutowiredFieldsWarningInspection", "Duplicates"})
@RestController
public class RegistrationController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private IUserService userService;
    @Qualifier("messageSource")
    @Autowired
    private MessageSource messages;
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private ApplicationEventPublisher eventPublisher;
    @Autowired
    private Environment env;
    @Autowired
    private AuthenticationManager authenticationManager;

    public RegistrationController() {
        super();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/api/registration", method = RequestMethod.POST)
    public User registerUserAccount(final @Valid UserDto accountDto, final HttpServletRequest request) {
        LOGGER.info("Registering user account with information: {}", accountDto);
        final User registered = userService.registerNewUserAccount(accountDto);
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale()));
        return registered;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/api/verify", method = RequestMethod.POST)
    public GenericResponse verifyUserAccount(@RequestParam("token") String token, final HttpServletRequest request) {
        LOGGER.info("Verifying user account with token: {}", token);
        User userByToken = userService.findUserByToken(token);
        if (userByToken == null)
            throw new TokenNotFoundException(messages.getMessage("message.tokenNotFound", null, request.getLocale()));
        userByToken.setEnabled(true);
        userService.saveRegisteredUser(userByToken);
        return new GenericResponse(String.valueOf(HttpStatus.OK.value()));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/api/sendtoken", method = RequestMethod.POST)
    public GenericResponse sendToken(@RequestParam("email") String email, final HttpServletRequest request) {
        LOGGER.info("Sending token to user account : {}", email);
        User userByEmail = userService.findUserByEmail(email);
        if (userByEmail == null)
            throw new UserNotFoundException(messages.getMessage("message.userNotFound", null, request.getLocale()));
        String token = UUID.randomUUID().toString();
        userByEmail.setToken(token);
        userService.saveRegisteredUser(userByEmail);
        eventPublisher.publishEvent(new OnSendTokenEvent(userByEmail, request.getLocale()));
        return new GenericResponse(String.valueOf(HttpStatus.OK.value()));
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/api/changepassword", method = RequestMethod.POST)
    public GenericResponse changePassword(final @Valid PasswordDto passwordDto, HttpServletRequest request) {
        String token = passwordDto.getToken();
        LOGGER.info("Changing user's password for token : {}", token);
        User userByToken = userService.findUserByToken(token);
        if (userByToken == null)
            throw new TokenNotFoundException(messages.getMessage("message.tokenNotFound", null, request.getLocale()));
        userByToken.setPassword(passwordEncoder.encode(passwordDto.getNewPassword()));
        userByToken.setToken(UUID.randomUUID().toString());
        userService.saveRegisteredUser(userByToken);
        return new GenericResponse(String.valueOf(HttpStatus.OK.value()));
    }


    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + StringUtils.COLON + request.getServerPort() + request.getContextPath();
    }

    public void authWithHttpServletRequest(HttpServletRequest request, String username, String password) {
        try {
            request.login(username, password);
        } catch (ServletException e) {
            LOGGER.error("Error while login ", e);
        }
    }

    public void authWithAuthManager(HttpServletRequest request, String username, String password) {
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username, password);
        authToken.setDetails(new WebAuthenticationDetails(request));
        Authentication authentication = authenticationManager.authenticate(authToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private void authWithoutPassword(User user) {
        List<Privilege> privileges = user.getRoles().stream().map(Role::getPrivileges).flatMap(Collection::stream).distinct().collect(Collectors.toList());
        List<GrantedAuthority> authorities = privileges.stream().map(p -> new SimpleGrantedAuthority(p.getName())).collect(Collectors.toList());
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
