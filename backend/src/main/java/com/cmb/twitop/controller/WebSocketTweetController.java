package com.cmb.twitop.controller;

import com.cmb.twitop.model.Tweet;
import com.cmb.twitop.service.IUserService;
import com.cmb.twitop.util.StringUtils;
import com.cmb.twitop.util.TasksExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

/*
 * Created by Azer on 1/25/2019.
 * Home Inc.
 */
@SuppressWarnings("unused")
@RestController
public class WebSocketTweetController {
    private final static Logger logger = LoggerFactory.getLogger(WebSocketTweetController.class);
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final IUserService userService;

    @Autowired
    public WebSocketTweetController(SimpMessagingTemplate simpMessagingTemplate, IUserService userService, SimpUserRegistry simpUserRegistry) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        logger.info(this.simpMessagingTemplate.getUserDestinationPrefix());
        this.userService = userService;
    }

    @RequestMapping(value = "/api/ws_tweets", method = RequestMethod.GET)
    @MessageMapping(value = "/ws_tweets")
    public String getTweets(Principal principal) {
        List<Tweet> tweets = TasksExecutor.getTweets(principal, userService, false);
        simpMessagingTemplate.convertAndSendToUser(principal.getName(), "/queue/tweets-update", tweets);
        return StringUtils.EMPTY;
    }

    @MessageExceptionHandler
    @SendToUser("/queue/errors")
    public String handleException(Throwable throwable)
    {
        return throwable.getMessage();
    }
}
