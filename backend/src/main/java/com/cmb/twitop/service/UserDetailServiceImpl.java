package com.cmb.twitop.service;

import com.cmb.twitop.dao.UserRepository;
import com.cmb.twitop.model.Role;
import com.cmb.twitop.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import java.util.Arrays;

/**
 * Created by Azer on 1/9/2019.
 * Home Inc.
 */
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User currentUser = userRepository.findByEmail(s);
        if (currentUser == null) {
            logger.error(String.format("User %s not found", s));
            throw new UsernameNotFoundException("User not found");
        }
        if (!currentUser.isEnabled()) {
            logger.error(String.format("User %s not active", currentUser.getEmail()));
            throw new UsernameNotFoundException("User not active");
        }
        String join = StringUtils.join(Arrays.asList(currentUser.getRoles().stream().map(Role::getName).toArray()), ',');
        return new org.springframework.security.core
                .userdetails.User(s, currentUser.getPassword()
                , true, true, true, true,
                AuthorityUtils.createAuthorityList(join));
    }
}
