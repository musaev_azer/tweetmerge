package com.cmb.twitop.service;

import com.cmb.twitop.dto.UserDto;
import com.cmb.twitop.exception.UserAlreadyExistException;
import com.cmb.twitop.model.User;

import java.util.Optional;

/**
 * Created by Azer on 12/18/2018.
 * Home Inc.
 */
@SuppressWarnings("unused")
public interface IUserService {
    User registerNewUserAccount(UserDto accountDto) throws UserAlreadyExistException;

    void saveRegisteredUser(User user);

    void deleteUser(User user);

    User findUserByToken(String token);

    User findUserByEmail(String email);

    Optional<User> getUserByID(long id);

    void changeUserPassword(User user, String password);

    boolean checkIfValidOldPassword(User user, String password);

}
