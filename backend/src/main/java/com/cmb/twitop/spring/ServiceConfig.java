package com.cmb.twitop.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "com.cmb.twitop.service" })
public class ServiceConfig {
}
