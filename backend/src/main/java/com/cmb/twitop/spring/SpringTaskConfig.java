package com.cmb.twitop.spring;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SuppressWarnings("SpringFacetCodeInspection")
@Configuration
@EnableScheduling
@ComponentScan({ "com.cmb.twitop.task" })
public class SpringTaskConfig {

}
