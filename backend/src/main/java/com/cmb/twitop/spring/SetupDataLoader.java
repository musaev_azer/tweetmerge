package com.cmb.twitop.spring;

import com.cmb.twitop.dao.PrivilegeRepository;
import com.cmb.twitop.dao.RoleRepository;
import com.cmb.twitop.dao.TweeterAccountRepository;
import com.cmb.twitop.dao.UserRepository;
import com.cmb.twitop.model.Privilege;
import com.cmb.twitop.model.Role;
import com.cmb.twitop.model.TweeterAccount;
import com.cmb.twitop.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@SuppressWarnings({"NullableProblems", "unused", "SpringJavaAutowiredFieldsWarningInspection", "SameParameterValue", "SpellCheckingInspection"})
@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadySetup = false;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Autowired
    private TweeterAccountRepository tweeterAccountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }
        final Privilege readPrivilege = createPrivilegeIfNotFound("READ_PRIVILEGE");
        final Privilege writePrivilege = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
        final Privilege passwordPrivilege = createPrivilegeIfNotFound("CHANGE_PASSWORD_PRIVILEGE");

        final List<Privilege> adminPrivileges = new ArrayList<>(Arrays.asList(readPrivilege, writePrivilege, passwordPrivilege));
        final List<Privilege> userPrivileges = new ArrayList<>(Arrays.asList(readPrivilege, passwordPrivilege));
        final Role adminRole = createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        createRoleIfNotFound("ROLE_USER", userPrivileges);

        ArrayList<TweeterAccount> tweeterAccounts = new ArrayList<>(Arrays.asList(
                createTweeterAccount("java"),
                createTweeterAccount("ssysfakb"),
                createTweeterAccount("miguyan2000"),
                createTweeterAccount("springboot"),
                createTweeterAccount("gooksel"),
                createTweeterAccount("aspnet"),
                createTweeterAccount("VisualStudio"),
                createTweeterAccount("gooksel"),
                createTweeterAccount("SavunmaSanayiST"),
                createTweeterAccount("Acemal71")));
        createUserIfNotFound("test@test.com", "_Password80", tweeterAccounts, new ArrayList<>(Collections.singletonList(adminRole)));

        alreadySetup = true;
    }

    @Transactional
    TweeterAccount createTweeterAccount(final String user) {
        TweeterAccount tweeterAccount = tweeterAccountRepository.findByUser(user);
        if (tweeterAccount == null) {
            tweeterAccount = new TweeterAccount();
            tweeterAccount.setUser(user);
            tweeterAccount = tweeterAccountRepository.save(tweeterAccount);
        }
        return tweeterAccount;
    }

    @Transactional
    Privilege createPrivilegeIfNotFound(final String name) {
        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege(name);
            privilege = privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    Role createRoleIfNotFound(final String name, final Collection<Privilege> privileges) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
        }
        role.setPrivileges(privileges);
        role = roleRepository.save(role);
        return role;
    }

    @Transactional
    void createUserIfNotFound(final String email, final String password, final Collection<TweeterAccount> tweeterAccounts, final Collection<Role> roles) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            user = new User();
            user.setPassword(passwordEncoder.encode(password));
            user.setEmail(email);
            user.setEnabled(true);
            user.setRoles(roles);
            user.setToken(UUID.randomUUID().toString());
            for (TweeterAccount tweeterAccount : tweeterAccounts) {
                user.addTweeterAccount(tweeterAccount);
            }
            userRepository.save(user);
        }
    }

}
