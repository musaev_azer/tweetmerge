package com.cmb.twitop.registration;

import com.cmb.twitop.model.User;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;

/**
 * Created by Azer on 1/11/2019.
 * Home Inc.
 */
public class BaseApplicationEvent extends ApplicationEvent {
    private final Locale locale;
    private final User user;

    BaseApplicationEvent(final User user, final Locale locale) {
        super(user);
        this.user = user;
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    public User getUser() {
        return user;
    }
}
