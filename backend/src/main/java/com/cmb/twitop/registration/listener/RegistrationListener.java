package com.cmb.twitop.registration.listener;

/*
  Created by Azer on 12/18/2018.
  Home Inc.
 */

import com.cmb.twitop.registration.OnRegistrationCompleteEvent;
import com.cmb.twitop.util.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


@SuppressWarnings({"SpringJavaAutowiredFieldsWarningInspection", "NullableProblems"})
@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {
    @Autowired
    MailSender mailSender;

    @Override
    public void onApplicationEvent(final OnRegistrationCompleteEvent event) {
        mailSender.setFromPropertyKey("support.email");
        mailSender.setSubjectPropertyKey("message.registrationConfirmation");
        mailSender.setMessagePropertyKey("message.registrationSuccess");
        mailSender.sendMail(event.getUser(), event.getLocale());
    }
}

