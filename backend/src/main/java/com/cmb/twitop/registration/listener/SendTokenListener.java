package com.cmb.twitop.registration.listener;

import com.cmb.twitop.registration.OnSendTokenEvent;
import com.cmb.twitop.util.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Created by Azer on 1/11/2019.
 * Home Inc.
 */
@SuppressWarnings({"SpringJavaAutowiredFieldsWarningInspection", "NullableProblems"})
@Component
public class SendTokenListener implements ApplicationListener<OnSendTokenEvent> {
    @Autowired
    MailSender mailSender;

    @Override
    public void onApplicationEvent(OnSendTokenEvent event) {
        mailSender.setFromPropertyKey("support.email");
        mailSender.setSubjectPropertyKey("message.resetPassword");
        mailSender.setMessagePropertyKey("message.resendToken");
        mailSender.sendMail(event.getUser(), event.getLocale());
    }
}
