package com.cmb.twitop.registration;

/*
  Created by Azer on 12/18/2018.
  Home Inc.
 */

import com.cmb.twitop.model.User;

import java.util.Locale;

@SuppressWarnings({"serial", "unused"})
public class OnRegistrationCompleteEvent extends BaseApplicationEvent {

    public OnRegistrationCompleteEvent(final User user, final Locale locale) {
        super(user, locale);
    }
}

