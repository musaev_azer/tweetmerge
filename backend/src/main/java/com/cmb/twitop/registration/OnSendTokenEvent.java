package com.cmb.twitop.registration;

import com.cmb.twitop.model.User;

import java.util.Locale;

/**
 * Created by Azer on 1/11/2019.
 * Home Inc.
 */
public class OnSendTokenEvent extends BaseApplicationEvent {

    public OnSendTokenEvent(final User user, final Locale locale) {
        super(user, locale);
    }
}
