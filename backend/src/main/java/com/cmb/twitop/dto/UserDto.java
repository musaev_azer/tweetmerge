package com.cmb.twitop.dto;

import com.cmb.twitop.validation.PasswordMatches;
import com.cmb.twitop.validation.ValidEmail;
import com.cmb.twitop.validation.ValidPassword;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@SuppressWarnings("unused")
@PasswordMatches
public class UserDto {
    @ValidEmail
    @NotNull
    @Size(min = 1, message = "{Size.userDto.email}")
    private String email;
    @NotNull
    @ValidPassword
    private String password;
    @NotNull
    @Size(min = 1)
    private String matchingPassword;

    private String phoneNumber;
    private String description;
    private Integer role;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(final Integer role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(final String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }


    @Override
    public String toString() {
        return "UserDto [email=" + email + ", password=" + password + ", matchingPassword=" + matchingPassword + ", role=" + role + "]";
    }

}
