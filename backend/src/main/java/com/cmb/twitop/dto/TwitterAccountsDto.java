package com.cmb.twitop.dto;

/**
 * Created by Azer on 1/21/2019.
 * Home Inc.
 */
public class TwitterAccountsDto {
    public String[] getAccounts() {
        return accounts;
    }

    public void setAccounts(String[] accounts) {
        this.accounts = accounts;
    }

    private String[] accounts;
}
