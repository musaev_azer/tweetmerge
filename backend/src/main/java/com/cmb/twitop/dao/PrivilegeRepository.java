package com.cmb.twitop.dao;

/**
 * Created by Azer on 12/18/2018.
 * Home Inc.
 */

import com.cmb.twitop.model.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;

@SuppressWarnings({"unused", "NullableProblems"})
public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {

    Privilege findByName(String name);

    @Override
    void delete(Privilege privilege);

}
