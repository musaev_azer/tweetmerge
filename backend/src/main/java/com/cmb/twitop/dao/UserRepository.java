package com.cmb.twitop.dao;

/*
  Created by Azer on 12/18/2018.
  Home Inc.
 */

import com.cmb.twitop.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

@SuppressWarnings({"unused", "NullableProblems"})
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    User findByToken(String token);

    @Override
    void delete(User user);

}
