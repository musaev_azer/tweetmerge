package com.cmb.twitop.dao;

import com.cmb.twitop.model.TweeterAccount;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Azer on 1/21/2019.
 * Home Inc.
 */
@SuppressWarnings({"NullableProblems", "unused"})
public interface TweeterAccountRepository extends JpaRepository<TweeterAccount, Long> {
    TweeterAccount findByUser(String user);
    TweeterAccount findByName(String name);

    @Override
    void delete(TweeterAccount tweeterAccount);
}
