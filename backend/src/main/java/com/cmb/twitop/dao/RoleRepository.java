package com.cmb.twitop.dao;

/**
 * Created by Azer on 12/18/2018.
 * Home Inc.
 */

import com.cmb.twitop.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

@SuppressWarnings({"unused", "NullableProblems"})
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);

    @Override
    void delete(Role role);

}
