package com.cmb.twitop.util;

import com.sendgrid.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by Azer on 1/11/2019.
 * Home Inc.
 */
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Component
@ConditionalOnProperty(name = "sendmail.usesendgrid", havingValue = "true")
public class SendGridMailSender implements IMailSender {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    SendGrid sendGrid;

    @Override
    public void sendMail(String from, String to, String subject, String body) {
        Email fromEmail = new Email(from);
        Email toEmail = new Email(to);
        Content content = new Content("text/plain", body);
        Mail mail = new Mail(fromEmail, subject, toEmail, content);
        try {
            Request request = new Request();
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sendGrid.api(request);
            logger.info("Sendgrid response = {}", response.getStatusCode());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}
