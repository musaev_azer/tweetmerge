package com.cmb.twitop.util;

/**
 * Created by Azer on 1/11/2019.
 * Home Inc.
 */
public interface IMailSender {
    void sendMail(String from, String to, String subject, String body);
}
