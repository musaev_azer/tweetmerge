package com.cmb.twitop.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

/**
 * Created by Azer on 1/11/2019.
 * Home Inc.
 */
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Component
@ConditionalOnProperty(name = "sendmail.usesendgrid", havingValue = "false")
public class JavaSpringMailSender implements IMailSender {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    org.springframework.mail.javamail.JavaMailSender javaMailSender;

    @Override
    public void sendMail(String from, String to, String subject, String body) {
        final SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(to);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(body);
        simpleMailMessage.setFrom(from);
        logger.info("Mail successfully sent.");
        try {
            javaMailSender.send(simpleMailMessage);
        } catch (MailException e) {
            logger.error(e.getMessage());
        }
    }
}
