package com.cmb.twitop.util;

import com.cmb.twitop.model.TweeterAccount;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Random;
import java.util.concurrent.Callable;

/*
 * Created by Azer on 1/21/2019.
 * Home Inc.
 */
public class QuickTweetsExtractor implements Callable {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static final String INIT_USER_URL = "https://twitter.com/%s";
    private static final String[] HEADER_LIST = {
            "Mozilla/5.0 (Windows; U; Windows NT 6.1; x64; fr; rv:1.9.2.13) Gecko/20101203 Firebird/3.6.13",
            "Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko",
            "Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201",
            "Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16",
            "Mozilla/5.0 (Windows NT 5.2; RW; rv:7.0a1) Gecko/20091211 SeaMonkey/9.23a1pre"
    };
    private final String user;

    public QuickTweetsExtractor(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public TweeterAccount call() throws Exception {
        String userAgent = HEADER_LIST[new Random().nextInt(HEADER_LIST.length - 1)];
        String url = String.format(INIT_USER_URL, user);
        URLConnection urlConnection = new URL(url).openConnection();
        urlConnection.setRequestProperty("User-Agent", userAgent);
        InputStream inputStream = urlConnection.getInputStream();
        String response = IOUtils.toString(inputStream, Charset.defaultCharset());
        Document document = Jsoup.parse(response);
        TweeterAccount tweeterAccount = new TweeterAccount();
        tweeterAccount.setUser(this.getUser());
        Element nameLink = document.select("a.ProfileHeaderCard-nameLink.u-textInheritColor.js-nav").first();
        if (nameLink != null) {
            tweeterAccount.setName(nameLink.text());
            tweeterAccount.setUrl(String.format("https://twitter.com%s", nameLink.attr("href")));
        }
        Elements elements = document.select("img.ProfileAvatar-image");
        Element profileImg = elements.first();
        if (profileImg != null) {
            String src = profileImg.attr("src");
            tweeterAccount.setImageUrl(src);
        }
        logger.info(tweeterAccount.toString());
        return tweeterAccount;
    }
}
