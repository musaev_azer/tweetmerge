package com.cmb.twitop.util;

import com.cmb.twitop.model.Tweet;
import com.cmb.twitop.model.TweeterAccount;
import com.cmb.twitop.model.User;
import com.cmb.twitop.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/*
 * Created by Azer on 1/25/2019.
 * Home Inc.
 */
@SuppressWarnings("unchecked")
public class TasksExecutor {
    private final static Logger logger = LoggerFactory.getLogger(TasksExecutor.class);
    private final static ExecutorService executorService = Executors.newFixedThreadPool(20);

    public static List<Tweet> getTweets(final Principal principal, IUserService userService, boolean scrapAll) {
        User user = requireNonNull(userService).findUserByEmail(principal.getName());
        List<String> accounts = user.getTweeterAccounts().stream()
                .map(TweeterAccount::getUser).collect(Collectors.toList());
        if(accounts.size() == 0)
            return new ArrayList<>();
        long currentTimeMillis = System.currentTimeMillis();
        List<Tweet> tweets = TasksExecutor.collectTasks(accounts, s -> new TweetsExtractor(s, scrapAll));
        float v = (System.currentTimeMillis() - currentTimeMillis) / 1000F;
        logger.info("Total {} tweets in {} seconds", tweets.size(), v);
        return tweets;
    }

    @SuppressWarnings("UnusedAssignment")
    private static <T> List<T> collectTasks(List<String> collection, Function<String, ? super Callable<List<T>>> function) {
        Collection<Callable<List<T>>> tasks = collection.stream()
                .map(s -> (Callable<List<T>>) function.apply(s)).collect(Collectors.toList());
        List<Future<List<T>>> futures = null;
        try {
            futures = executorService.invokeAll(tasks);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
        List<T> tList = requireNonNull(futures).stream().map(future -> {
            try {
                return future.get();
            } catch (InterruptedException | ExecutionException e) {
                logger.error(e.getMessage());
            }
            return null;
        }).collect(Collectors.toList()).stream().flatMap(List::stream).collect(Collectors.toList());
        futures = null;
        tasks = null;
        return tList;
    }

}
