package com.cmb.twitop.util;

import com.cmb.twitop.model.Tweet;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Created by Azer on 1/15/2019.
 * Home Inc.
 */
@SuppressWarnings({"Duplicates", "UnusedAssignment"})
public class TweetsExtractor implements Callable {
    private static final int LIMIT = 2000;
    private static final String INIT_USER_URL = "https://twitter.com/%s";
    private static final String RELOAD_USER_URL = "https://twitter.com/i/profiles/show/%s/timeline/tweets?include_available_features=1&include_entities=1&max_position=%s&reset_error_state=false";
    private static final String[] HEADER_LIST = {
            "Mozilla/5.0 (Windows; U; Windows NT 6.1; x64; fr; rv:1.9.2.13) Gecko/20101203 Firebird/3.6.13",
            "Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko",
            "Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201",
            "Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16",
            "Mozilla/5.0 (Windows NT 5.2; RW; rv:7.0a1) Gecko/20091211 SeaMonkey/9.23a1pre"
    };
    private final String user;
    private final boolean scrapAll;
    private final Logger logger = LoggerFactory.getLogger(getClass());


    TweetsExtractor(String user, boolean scrapAll) {
        this.user = user;
        this.scrapAll = scrapAll;
    }

    @Override
    public List<Tweet> call() {
        try {
            return queryTweetsFromUser(this.user);
        } catch (IOException e) {
            logger.error(e.getMessage());
            return  new ArrayList<>();
        }
    }

    private List<Tweet> queryTweetsFromUser(String user) throws IOException {
        if (!this.scrapAll) {
            Pair<List<Tweet>, String> listStringPair = querySinglePage(String.format(INIT_USER_URL, user), true);
            return listStringPair.getLeft();
        }
        List<Tweet> tweets = new ArrayList<>();
        String position = StringUtils.EMPTY;
        try {
            while (true) {
                String url = position.equals(StringUtils.EMPTY) ? String.format(INIT_USER_URL, user) : String.format(RELOAD_USER_URL, user, position);
                Pair<List<Tweet>, String> listStringPair = querySinglePage(url, position.equals(StringUtils.EMPTY));
                if (listStringPair.getLeft().size() == 0) {
                    logger.info("Scraped {} tweets from username {}", tweets.size(), user);
                    return tweets;
                }
                position = listStringPair.getRight();
                tweets.addAll(listStringPair.getLeft());
                if (tweets.size() >= LIMIT || position == null) {
                    logger.info("Scraped {} tweets from username {}", tweets.size(), user);
                    return tweets;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return tweets;
    }

    private Pair<List<Tweet>, String> querySinglePage(String url, boolean htmlRead) throws IOException {
        ArrayList<Tweet> tweets = new ArrayList<>();
        String position = null;
        String userAgent = HEADER_LIST[new Random().nextInt(HEADER_LIST.length - 1)];
        URLConnection urlConnection = null;
        InputStream inputStream = null;
        String response = StringUtils.EMPTY;
        try {
            urlConnection = new URL(url).openConnection();
            urlConnection.setRequestProperty("User-Agent", userAgent);
            inputStream = urlConnection.getInputStream();
            response = IOUtils.toString(inputStream, Charset.defaultCharset());
            if (htmlRead) {
                tweets = (ArrayList<Tweet>) toTweets(response);
                position = tweets.get(tweets.size() - 1).getId();

            } else {
                JSONObject jsonObject = new JSONObject(response);
                logger.info(jsonObject.toString());
                tweets = (ArrayList<Tweet>) toTweets(jsonObject.getString("items_html"));
                position = jsonObject.getString("min_position");
            }

        } catch (IOException | JSONException e) {
            logger.error(e.getMessage());
        }
        finally {
            if(inputStream != null)
                inputStream.close();
            inputStream = null;
            urlConnection = null;
            response = null;
            System.gc();
        }
        return new ImmutablePair<>(tweets, position);
    }

    private List<Tweet> toTweets(String html) {
        ArrayList<Tweet> tweets = new ArrayList<>();
        Document document = Jsoup.parse(html);
        Elements elements = document.select("li.js-stream-item");
        for (Element element : elements) {
            Tweet tweet = new Tweet();
            tweet.setUser(element.select("span.username").text());
            tweet.setFullName(element.select("strong.fullname").text());
            tweet.setId(element.attr("data-item-id"));
            tweet.setUrl(element.select("div.tweet").attr("data-permalink-path"));
            String timestamp = element.select("span._timestamp").attr("data-time");
            try {
                tweet.setTimestamp(new Date(Timestamp.from(Instant.ofEpochSecond(Long.valueOf(timestamp))).getTime()));
            } catch (NumberFormatException e) {
                tweet.setTimestamp(new Date(Long.MIN_VALUE));
            }
            tweet.setText(element.select("p.tweet-text").text());
            String attr = element.select("span.ProfileTweet-action--reply.u-hiddenVisually")
                    .select("span.ProfileTweet-actionCount").attr("data-tweet-stat-count");
            tweet.setReplies(Integer.valueOf(attr.equals("") ? "0" : attr));
            attr = element.select("span.ProfileTweet-action--retweet.u-hiddenVisually")
                    .select("span.ProfileTweet-actionCount").attr("data-tweet-stat-count");
            tweet.setRetweets(Integer.valueOf(attr.equals("") ? "0" : attr));
            attr = element.select("span.ProfileTweet-action--favorite.u-hiddenVisually")
                    .select("span.ProfileTweet-actionCount").attr("data-tweet-stat-count");
            tweet.setLikes(Integer.valueOf(attr.equals("") ? "0" : attr));
            tweet.setHtml(element.select("p.tweet-text").html());
            tweet.setImageUrl(element.select("img.avatar.js-action-profile-avatar").attr("src"));
            tweets.add(tweet);
            Element mediaOuterContainer = element.select("div.AdaptiveMediaOuterContainer").first();
            if (mediaOuterContainer != null) {
                String adaptiveMediaImageUrl = mediaOuterContainer.select("img").attr("src");
                if (!adaptiveMediaImageUrl.equals(""))
                    tweet.setAdaptiveImageUrl(adaptiveMediaImageUrl);
                Element divAdaptiveMediaPhoto = mediaOuterContainer
                        .select("div.AdaptiveMedia-doublePhoto, div.AdaptiveMedia-triplePhoto, div.AdaptiveMedia-quadPhoto").first();
                if (divAdaptiveMediaPhoto != null) {
                    Elements images = divAdaptiveMediaPhoto.getElementsByTag("img");
                    for (Element image : images) tweet.getImages().add(image.attr("src"));
                }
                Elements video = element.select("video[preload=\"none\"]");
                if (video != null) {
                    String adaptiveMediaVideoSrcUrl = video.attr("src");
                    if (!adaptiveMediaVideoSrcUrl.equals("")) {
                        String adaptiveMediaPosterUrl = mediaOuterContainer.select("video").attr("poster");
                        tweet.setAdaptiveVideoUrl(adaptiveMediaVideoSrcUrl);
                        tweet.setAdaptivePosterUrl(adaptiveMediaPosterUrl);
                    }
                }
            }
            logger.info(tweet.toString());
        }
        document = null;
        elements.clear();
        elements = null;
        return tweets;
    }

}
