package com.cmb.twitop.util;

import com.cmb.twitop.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Created by Azer on 1/11/2019.
 * Home Inc.
 */
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Component
public class MailSender {
    private static final Logger logger = LoggerFactory.getLogger(MailSender.class);
    @Autowired(required = false)
    JavaSpringMailSender javaMailSender;
    @Autowired(required = false)
    SendGridMailSender sendGridMailSender;
    @Autowired
    MailSender mailSender;
    @Qualifier("messageSource")
    @Autowired
    private MessageSource messages;
    @Autowired
    private Environment env;
    private String subjectPropertyKey;
    private String fromPropertyKey;
    private String messagePropertyKey;

    public void sendMail(User user, Locale locale) {
        boolean useSendGrid = Boolean.parseBoolean(env.getProperty("sendmail.usesendgrid"));
        final String token = user.getToken();
        final String message = messages.getMessage(getMessagePropertyKey(), null, locale);
        String from = env.getProperty(getFromPropertyKey());
        String subject = messages.getMessage(getSubjectPropertyKey(), null, locale);
        String body = message + " \r\n" + token;
        if (useSendGrid) sendGridMailSender.sendMail(from, user.getEmail(), subject, body);
        else javaMailSender.sendMail(from, user.getEmail(), subject, body);
        logger.info("Sent mail to {} with token {}", user.getEmail(), token);
    }

    private String getSubjectPropertyKey() {
        return subjectPropertyKey;
    }

    public void setSubjectPropertyKey(String subjectPropertyKey) {
        this.subjectPropertyKey = subjectPropertyKey;
    }

    private String getFromPropertyKey() {
        return fromPropertyKey;
    }

    public void setFromPropertyKey(String fromPropertyKey) {
        this.fromPropertyKey = fromPropertyKey;
    }

    private String getMessagePropertyKey() {
        return messagePropertyKey;
    }

    public void setMessagePropertyKey(String messagePropertyKey) {
        this.messagePropertyKey = messagePropertyKey;
    }
}


