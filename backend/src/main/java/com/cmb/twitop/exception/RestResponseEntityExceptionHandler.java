package com.cmb.twitop.exception;

import com.cmb.twitop.util.GenericResponse;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@SuppressWarnings({"SpringJavaAutowiredFieldsWarningInspection", "NullableProblems", "Duplicates"})
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Qualifier("messageSource")
    @Autowired
    private MessageSource messages;

    public RestResponseEntityExceptionHandler() {
        super();
    }

    @Override
    protected ResponseEntity<Object> handleBindException(final BindException bindingException, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.error("400 Status Code", bindingException);
        final BindingResult result = bindingException.getBindingResult();
        final GenericResponse bodyOfResponse = new GenericResponse(result.getAllErrors(), "Invalid" + result.getObjectName());
        return handleExceptionInternal(bindingException, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException exception, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.error("400 Status Code", exception);
        final BindingResult result = exception.getBindingResult();
        final GenericResponse bodyOfResponse = new GenericResponse(result.getAllErrors(), "Invalid" + result.getObjectName());
        return handleExceptionInternal(exception, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({ExpiredJwtException.class})
    public ResponseEntity<Object> handleExpiredJwtToken(final RuntimeException ex, final WebRequest request) {
        return handleCustomException(ex, request, "ExpiredJwtToken", "message.tokenExpired", HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({UserNotFoundException.class})
    public ResponseEntity<Object> handleUserNotFound(final RuntimeException ex, final WebRequest request) {
        return handleCustomException(ex, request, "UserNotFound", "message.userNotFound", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({UserInActiveException.class})
    public ResponseEntity<Object> handleUserInActive(final RuntimeException ex, final WebRequest request) {
        return handleCustomException(ex, request, "UserNotActive", "message.userNotActive", HttpStatus.EXPECTATION_FAILED);
    }

    @ExceptionHandler({UserAlreadyExistException.class})
    public ResponseEntity<Object> handleUserAlreadyExist(final RuntimeException ex, final WebRequest request) {
        return handleCustomException(ex, request, "UserAlreadyExist", "message.registrationError", HttpStatus.EXPECTATION_FAILED);
    }

    @ExceptionHandler({TokenNotFoundException.class})
    public ResponseEntity<Object> handleTokenNotFound(final RuntimeException ex, final WebRequest request) {
        return handleCustomException(ex, request, "TokenNotFound", "message.tokenNotFound", HttpStatus.EXPECTATION_FAILED);
    }

    @ExceptionHandler({RegistrationFailedException.class})
    public ResponseEntity<Object> handleRegistrationFailed(final RuntimeException ex, final WebRequest request) {
        return handleCustomException(ex, request, "RegistrationFailed", "message.registrationFailed", HttpStatus.EXPECTATION_FAILED);
    }

    @ExceptionHandler({MailAuthenticationException.class})
    public ResponseEntity<Object> handleMail(final RuntimeException ex, final WebRequest request) {
        logger.error("500 Status Code", ex);
        final GenericResponse bodyOfResponse = new GenericResponse(messages.getMessage("message.email.config.error", null, request.getLocale()), "MailError");
        return new ResponseEntity<>(bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleInternal(final RuntimeException ex, final WebRequest request) {
        logger.error("500 Status Code", ex);
        final GenericResponse bodyOfResponse = new GenericResponse(messages.getMessage("message.error", null, request.getLocale()), "InternalError");
        return new ResponseEntity<>(bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<Object> handleCustomException(final RuntimeException ex, final WebRequest request, String error, String message, HttpStatus httpStatus) {
        logger.error(String.format("%d Status Code: %s", httpStatus.value(), ex.getMessage()));
        final GenericResponse bodyOfResponse = new GenericResponse(messages.getMessage(message, null, request.getLocale()), error);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), httpStatus, request);
    }

}
