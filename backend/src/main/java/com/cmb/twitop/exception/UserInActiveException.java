package com.cmb.twitop.exception;

/**
 * Created by Azer on 1/9/2019.
 * Home Inc.
 */
@SuppressWarnings("unused")
public class UserInActiveException extends RuntimeException {
    public UserInActiveException() {
        super();
    }

    public UserInActiveException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UserInActiveException(final String message) {
        super(message);
    }

    public UserInActiveException(final Throwable cause) {
        super(cause);
    }
}
