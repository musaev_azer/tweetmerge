package com.cmb.twitop.exception;

/**
 * Created by Azer on 1/10/2019.
 * Home Inc.
 */
@SuppressWarnings("unused")
public class TokenNotFoundException extends RuntimeException {
    public TokenNotFoundException() {
        super();
    }

    public TokenNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TokenNotFoundException(final String message) {
        super(message);
    }

    public TokenNotFoundException(final Throwable cause) {
        super(cause);
    }
}
