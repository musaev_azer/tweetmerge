package com.cmb.twitop.exception;

/**
 * Created by Azer on 1/7/2019.
 * Home Inc.
 */
@SuppressWarnings("unused")
public class RegistrationFailedException extends RuntimeException {

    public RegistrationFailedException() {
        super();
    }

    public RegistrationFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public RegistrationFailedException(final String message) {
        super(message);
    }

    public RegistrationFailedException(final Throwable cause) {
        super(cause);
    }
}
