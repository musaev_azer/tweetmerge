package com.cmb.twitop.filter;

import com.cmb.twitop.model.AccountCredential;
import com.cmb.twitop.service.AuthenticationService;
import com.cmb.twitop.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

/**
 * Created by Azer on 1/9/2019.
 * Home Inc.
 */
@SuppressWarnings({"unused", "SpringJavaAutowiredMembersInspection"})
public class LoginFilter extends AbstractAuthenticationProcessingFilter {
    private final static Logger logger = LoggerFactory.getLogger(LoginFilter.class);

    @Autowired
    UserService userService;

    public LoginFilter(String url, AuthenticationManager authenticationManager) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authenticationManager);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) {
        AuthenticationService.addToken(response, authResult.getName());
        logger.info("User {} successfully authenticated", authResult.getName());
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException {
        AccountCredential accountCredential = new ObjectMapper().readValue(request.getInputStream(), AccountCredential.class);
        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(accountCredential.getEmail(), accountCredential.getPassword(), Collections.emptyList()));
    }
}
