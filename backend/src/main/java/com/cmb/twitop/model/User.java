package com.cmb.twitop.model;

/*
  Created by Azer on 12/18/2018.
  Home Inc.
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("unused")
@Entity
@Table(name = "user_account")
public class User {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    private Long id;

    @NotBlank
    @Email
    @Column(nullable = false, unique = true)
    private String email;

    @Column(length = 60)
    @NotBlank
    @JsonIgnore
    private String password;

    @NotBlank
    @Column(nullable = false, unique = true)
    @JsonIgnore
    private String token;

    private boolean enabled;

    private String phoneNumber;

    private String description;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    @JsonManagedReference
    private Collection<Role> roles;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parentUser", orphanRemoval = true)
    @JsonManagedReference
    private Collection<TweeterAccount> tweeterAccounts = new ArrayList<>();

    public User() {
        super();
        this.enabled = false;
    }

    public void addTweeterAccount(TweeterAccount tweeterAccount) {
        tweeterAccounts.add(tweeterAccount);
        tweeterAccount.setParentUser(this);
    }

    public void removeTweeterAccount(TweeterAccount tweeterAccount) {
        tweeterAccounts.remove(tweeterAccount);
        tweeterAccount.setParentUser(null);
    }

    public Collection<TweeterAccount> getTweeterAccounts() {
        return tweeterAccounts;
    }

    public void setTweeterAccounts(Collection<TweeterAccount> tweeterAccounts) {
        this.tweeterAccounts = tweeterAccounts;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String username) {
        this.email = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(final Collection<Role> roles) {
        this.roles = roles;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((email == null) ? 0 : email.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User user = (User) obj;
        return email.equals(user.email);
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", email=" + email + ", password=" + password + ", enabled=" + enabled +
                ", roles=" + roles + ", phone=" + phoneNumber + ", description=" + description + "]";
    }

}
