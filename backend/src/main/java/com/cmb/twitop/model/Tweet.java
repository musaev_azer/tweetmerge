package com.cmb.twitop.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 * Created by Azer on 1/15/2019.
 * Home Inc.
 */
@SuppressWarnings({"unused"})
public class Tweet implements Serializable {
    private String user;
    private String fullName;
    private String id;
    private String url;
    private Date timestamp;
    private String text;
    private int replies;
    private int retweets;
    private int likes;
    private String html;
    private String imageUrl;
    private String adaptiveImageUrl;
    private String adaptiveVideoUrl;
    private String adaptivePosterUrl;
    private List<String> images;

    public Tweet() {
        this.images = new ArrayList<>();
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getAdaptiveImageUrl() {
        return adaptiveImageUrl;
    }

    public void setAdaptiveImageUrl(String adaptiveImageUrl) {
        this.adaptiveImageUrl = adaptiveImageUrl;
    }

    public String getAdaptiveVideoUrl() {
        return adaptiveVideoUrl;
    }

    public void setAdaptiveVideoUrl(String adaptiveVideoUrl) {
        this.adaptiveVideoUrl = adaptiveVideoUrl;
    }

    public String getAdaptivePosterUrl() {
        return adaptivePosterUrl;
    }

    public void setAdaptivePosterUrl(String adaptivePosterUrl) {
        this.adaptivePosterUrl = adaptivePosterUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "user='" + user + '\'' +
                ", fullName='" + fullName + '\'' +
                ", id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", timestamp=" + timestamp +
                ", text='" + text + '\'' +
                ", replies=" + replies +
                ", retweets=" + retweets +
                ", likes=" + likes +
                ", imageUrl='" + imageUrl + '\'' +
                ", adaptiveImageUrl='" + adaptiveImageUrl + '\'' +
                ", adaptiveVideoUrl='" + adaptiveVideoUrl + '\'' +
                ", adaptivePosterUrl='" + adaptivePosterUrl + '\'' +
                '}';
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getReplies() {
        return replies;
    }

    public void setReplies(int replies) {
        this.replies = replies;
    }

    public int getRetweets() {
        return retweets;
    }

    public void setRetweets(int retweets) {
        this.retweets = retweets;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}
