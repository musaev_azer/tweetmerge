package com.cmb.twitop.model;

/**
 * Created by Azer on 1/9/2019.
 * Home Inc.
 */
public class AccountCredential {
    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
