package com.cmb.twitop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SuppressWarnings("SpringBootApplicationSetup")
@SpringBootApplication
@ComponentScan(basePackages = {"com.cmb.twitop"})
@EnableScheduling
public class TwitopApplication {

    public static void main(String[] args) {
        SpringApplication.run(TwitopApplication.class, args);
    }

}

